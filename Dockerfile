FROM php:7.1-apache

RUN apt-get update \
  && apt-get install -y curl libxml2-dev zlib1g-dev git unzip \
  && docker-php-ext-install mbstring pdo tokenizer xml zip \
  && apt-get clean

RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
COPY html/ /var/www/html/
#WORKDIR  /var/www/html/blog/
#RUN composer install
