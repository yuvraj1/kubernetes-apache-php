#!/usr/bin/env bash
TAG=${1}

export BUILD_NUMBER=${TAG}

mkdir -p .generated

for f in kube-yaml-files/*.yaml
do
 envsubst < $f > ".generated/$(basename $f)"
done
kubectl apply -f .generated/